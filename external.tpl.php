<?php // $Id$
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $layout_settings; ?>
  <?php print $scripts; ?>
</head>

<body id="external" style="margin: 0 !important; padding: 0 !important;">
<?php if ($messages): ?>
  <?php print $messages; ?>
<?php endif; ?>
<div id="external-container">
  <div class="hide-toolbar">
    <a href="<?php print $url;?>">CLOSE &nbsp;&nbsp;X</a></span>
  </div>
  <div class="logo" id="community-nav">
    <a href="<?php print $front_page; ?>" title="Return home"><img src="<?php print $logo; ?>"></a>
  </div>
</div>
<div id="external-site-container" height="100%">
  <iframe id="external-site" src="<?php print $url; ?>" scrolling="auto" frameBorder="0" height="100%" />
    <h3>Your Browser Does Not Support Iframes. <a href="<?php print $url; ?>" title="<?php print $url; ?>">Click here to view the page you selected</a></h3>
  </iframe>
</div>

  <?php print $closure ?>
</body>
</html>
